import sys
import os


def graphInsert(g, id1, id2):
	"""insert edge id1:id2 into graph
	graph is a dictionary of sets where the key is a id and the set
	corresponding is all the id it has had interactions with"""
	if id1 in g.keys():
		g[id1].add(id2)
	else:
		g[id1] = set([id2])
	if id2 in g.keys():
		g[id2].add(id1)
	else:
		g[id2] = set([id1])


def checkDistance(g, id1, id2, n):
	"""if id2 is n neighbors away from id1 return true (trusted)"""
	if id1 not in g.keys() or id2 not in g.keys():
		return False
	# only have to check n levels bfs
	if id2 not in bfs(g, id1, n):
		return False
	else:
		return True


def bfs(graph, start, n):
	"""traverse all nodes within depth n from st"""
	visited = set()
	q = [start]
	level = [0]
	while len(q) > 0:
		v = q.pop(0)
		l = level.pop(0)
		if l > n:
			return visited
		if v not in visited:
			visited.add(v)
			q.extend(graph[v] - visited)
			level.extend([l + 1] * len(graph[v] - visited))
	return visited


def main(input_file, input_file2, output1, output2, output3):
	out1 = open(output1, 'w')
	out2 = open(output2, 'w')
	out3 = open(output3, 'w')
	options = ("unverified", "trusted")
	g = {}
	with open(input_file, "r") as data_file:
		# insert batch files into graph
		lines = data_file.readlines()
		if len(lines) > 0:
			if len(lines[0].split(", ")) > 0:
				n = len(lines[0].split(", "))
				id1_ind = lines[0].split(", ").index("id1")
				id2_ind = lines[0].split(", ").index("id2")
				for line in lines[1:len(lines)]:
					elements = line.split(', ')
					if len(elements) < n:
						raise ValueError("Invalid csv format")
					id1 = elements[id1_ind]
					id2 = elements[id2_ind]
					if id1 is None or id2 is None:
						raise ValueError("csv file contains invalid")
					if id1 is not None and id2 is not None:
						graphInsert(g, id1, id2)
	with open(input_file2, "r") as data_file2:
		# check graph for closeness
		# update graph
		lines = data_file2.readlines()
		if len(lines[0]) < 1:
			return
		n = len(lines[0].split(", "))
		id1_ind = lines[0].split(", ").index("id1")
		id2_ind = lines[0].split(", ").index("id2")
		for line in lines[1:len(lines)]:
			elements = line.split(', ')
			if len(elements) < n:
				print elements
				raise ValueError("Invalid csv format")
			id1 = elements[id1_ind]
			id2 = elements[id2_ind]
			if id1 is None or id2 is None:
				raise ValueError("csv file contains invalid")
			if id1 is not None and id2 is not None:
				# write feature values to text file
				feat1 = checkDistance(g, id1, id2, 1)
				if not feat1:
					feat2 = checkDistance(g, id1, id2, 2)
					if not feat2:
						feat3 = checkDistance(g, id1, id2, 4)
					else:
						feat3 = feat2
				else:
					feat2 = feat1
					feat3 = feat1
				out1.write(options[feat1] + '\n')
				out2.write(options[feat2] + '\n')
				out3.write(options[feat3] + '\n')
				graphInsert(g, id1, id2)


if __name__ == '__main__':
	input_txt = sys.argv[1]
	input_txt2 = sys.argv[2]
	output1 = sys.argv[3]
	output2 = sys.argv[4]
	output3 = sys.argv[5]
	main(input_txt, input_txt2, output1, output2, output3)
